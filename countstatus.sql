DROP PROCEDURE IF EXISTS CountOrderByStatus;

DELIMITER $$


CREATE PROCEDURE CountOrderByStatus(IN OrderStatus varchar(50) , out Total INT)
BEGIN
	select count(orderNumber)
	INTO Total 
	from orders where status=OrderStatus;

END $$

DELIMITER ;
