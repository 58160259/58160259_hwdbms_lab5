DROP PROCEDURE IF EXISTS StatusByCustomer;

DELIMITER $$

CREATE PROCEDURE StatusByCustomer(IN OrderNumber INT, IN CustomerNumber INT, out OrderStatus varchar(20))
BEGIN
	SELECT status
	INTO OrderStatus
	from orders 
	where orderNumber=OrderNumber and customerNumber=CustomerNumber
	LIMIT 1;


END $$


DELIMITER ;
